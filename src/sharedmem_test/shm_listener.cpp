#include "ros/ros.h"
#include "std_msgs/String.h"

#include <ros/sharedmem_transport/sharedmem_node_handle.h>

void chatterCallback(const std_msgs::String::ConstPtr & msg)
{
	ROS_INFO("I heard [%s]", msg->data.c_str());
}

int main(int argc, char ** argv)
{
	ros::init(argc, argv, "shm_listener", ros::init_options::AnonymousName);
	ros::NodeHandle n;
	sharedmem_transport::NodeHandle t(n);
	sharedmem_transport::Subscriber<std_msgs::String> s = t.subscribe("/sharedmemtest", chatterCallback);
	ros::spin();
	return 0;
}

