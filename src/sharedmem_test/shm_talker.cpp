#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <fstream>

#include <ros/sharedmem_transport/sharedmem_node_handle.h>

int main(int argc, char ** argv)
{
	ros::init(argc, argv, "shm_talker", ros::init_options::AnonymousName);
	ros::NodeHandle n;
	sharedmem_transport::NodeHandle t(n);
	sharedmem_transport::Publisher p = t.advertise("/sharedmemtest", 1024*1024*10);

	ros::Rate loop_rate(30);
	int count = 0;
	while (count < 20)
	{

		/*std_msgs::String msg;
		std::ifstream in("/home/liendong/message/input1M.txt");
		std::ostringstream ss;
		char ch;
		while(ss && in.get(ch))
		{
			ss.put(ch);
		}
		msg.data = ss.str();*/

		std_msgs::String msg;
		std::stringstream ss;
		ss<<"Hello ROS "<<count;
		msg.data = ss.str();

		ROS_INFO("I public [%s]", msg.data.c_str());

		p.publish(msg);

		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
	return 0;
}

