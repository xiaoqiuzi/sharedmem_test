#include <ros/ros.h>
#include "std_msgs/String.h"
#include <sstream>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <ros/sharedmem_transport/sharedmem_node_handle.h>

int count = 0;

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
	try
	{
		std_msgs::String out_msg;
		std::stringstream ss;
		cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);

		ss << "the " << ++count<< "th frame I send!";
		out_msg.data = ss.str();
		ROS_INFO("%s", out_msg.data.c_str());
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "sharedmem_image_listener", ros::init_options::AnonymousName);
	ros::NodeHandle nh;
	cv::namedWindow("view");
	cv::startWindowThread();
	sharedmem_transport::NodeHandle it(nh);
	sharedmem_transport::Subscriber<sensor_msgs::Image> sub = it.subscribe("/sharedmem_image", imageCallback);
  	ros::spin();
  	cv::destroyWindow("view");
}
