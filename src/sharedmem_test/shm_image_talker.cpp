#include <ros/ros.h>
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <ros/sharedmem_transport/sharedmem_node_handle.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "sharedmem_image_publisher");
	ros::NodeHandle nh;
	sharedmem_transport::NodeHandle it(nh);
	sharedmem_transport::Publisher pub = it.advertise("/sharedmem_image", 1024*1024*10);

	cv::VideoCapture cap("/home/liendong/Videos/test");
	cv::Mat frame;
	sensor_msgs::ImagePtr frame_msg;
	ros::Rate loop_rate(30);

	int count = 0;

	while(ros::ok())
	{
		cap >> frame;
		if (!frame.empty())
		{
			frame_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
			pub.publish(*frame_msg);

			std_msgs::String msg;
			std::stringstream ss;
			ss << "the " << ++count<< "th frame I send!";
			msg.data = ss.str();
			ROS_INFO("%s", msg.data.c_str());
			cv::waitKey(1);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
}
